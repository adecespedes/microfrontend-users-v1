import _ from "lodash";
import { APP_CONSTANTS } from "./constants/";
import i18n from "./plugins/i18n";

const StatsPlugin = require("stats-webpack-plugin");
const webpack = require("webpack");

export default {
  globalName: "subapp",
  globals: {
    id: (globalName) => `__subapp`,
    nuxt: (globalName) => `$${globalName}`,
    context: (globalName) => `__${globalName.toUpperCase()}__`,
    pluginPrefix: (globalName) => globalName,
    readyCallback: (globalName) => `on${_.capitalize(globalName)}Ready`,
    loadedCallback: (globalName) => `_on${_.capitalize(globalName)}Loaded`,
  },
  telemetry: { enabled: true, consent: true },
  publicRuntimeConfig: {
    appName: APP_CONSTANTS.APP_NAME,
    appVersion: APP_CONSTANTS.APP_VERSION,
    appRoleAdmin: APP_CONSTANTS.APP_ROLE_ADMIN,
    appHomeUrl: APP_CONSTANTS.APP_HOME,
  },
  ssr: false,
  mode: "spa",
  router: {
    mode: "hash",
  },
  loading: { color: "#fff" },
  /*
   ** Global CSS
   */
  css: ["./assets/scss/main.scss"],
  buildModules: [
    "@nuxtjs/eslint-module",
    "@nuxtjs/composition-api/module",
    "@nuxtjs/device",
  ],
  /*
   ** Nuxt.js modules
   */
  MFE: {
    force: true,

    path: "./mfe.js",
    output: {
      library: `subapp`,
      libraryTarget: "umd",
    },
  },
  modules: [
    "@femessage/nuxt-micro-frontend",
    "@nuxtjs/axios",
    "@nuxtjs/pwa",
    "nuxt-buefy",
    [
      "@nuxtjs/i18n",
      {
        defaultLocale: "es",
        locales: ["es", "en"],
        strategy: "no_prefix",
        vueI18n: i18n,
        vueI18nLoader: true,
        detectBrowserLanguage: false,
        parsePages: false,
        silentTranslationWarn: true,
        silentFallbackWarn: true,
      },
    ],
  ],
  plugins: [
    { src: "~/plugins/global-error-handler.js", mode: "client" },
    { src: "~/plugins/premium-tesh-ui-lib.js", mode: "client" },
    { src: "~/plugins/after-each.js", mode: "client" },
    { src: "~/plugins/axios.js", mode: "client" },
  ],
  components: ["~/components"],
  axios: {
    proxy: true, // Can be also an object with default options
  },
  proxy: {
    "/api-data/": "https://krystalnews.premiumtesh.nat.cu",
  },
  pwa: {
    meta: {
      title: APP_CONSTANTS.APP_NAME,
      author: "PremiumTesh",
    },
    manifest: {
      name: APP_CONSTANTS.APP_NAME_EXTENDED,
      short_name: APP_CONSTANTS.APP_NAME,
      lang: "en",
      display: "standalone",
    },
  },
  /*
   ** Build configuration
   */

  build: {
    /*
     ** You can extend webpack config here
     */
    minimize: false,
    minimizer: [
      // terser-webpack-plugin
      // optimize-css-assets-webpack-plugin
    ],
    plugins: [
      new webpack.optimize.LimitChunkCountPlugin({
        maxChunks: 4,
      }),
    ],
    extractCSS: false,
    optimization: {
      splitChunks: {
        chunks: "async",
        maxAsyncRequests: 1,
        maxInitialRequests: 1,
      },
      runtimeChunk: false,
    },

    splitChunks: {
      chunks: "async",
      maxAsyncRequests: 1,
      maxInitialRequests: 1,
      automaticNameDelimiter: ".",
      name: false,
      cacheGroups: {
        default: false,
      },
      pages: false,
      vendor: false,
      commons: false,
      runtime: false,
      layouts: false,
    },
    filenames: {
      app: "subapp.js",
    },
    extend(config, { isDev, isClient }) {
      config.plugins.push(
        new StatsPlugin("manifest.json", {
          chunkModules: false,
          entrypoints: true,
          source: false,
          chunks: false,
          modules: false,
          assets: false,
          children: false,
          exclude: [/node_modules/],
        })
      );
    },
  },
};
