const cookies = {
  // returns the cookie with the given name,
  // or undefined if not found
  getCookie(name) {
    const matches = document.cookie.match(
      new RegExp(
        '(?:^|; )' +
          // eslint-disable-next-line no-useless-escape
          name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') +
          '=([^;]*)'
      )
    )
    return matches ? decodeURIComponent(matches[1]) : undefined
  },
  setCookie(name, value, options = { 'max-age': 86400 }) {
    options = {
      path: '/',
      // add other defaults here if necessary
      ...options
    }

    if (options.expires instanceof Date) {
      options.expires = options.expires.toUTCString()
    }

    let updatedCookie =
      encodeURIComponent(name) + '=' + encodeURIComponent(value)

    for (const optionKey in options) {
      updatedCookie += '; ' + optionKey
      const optionValue = options[optionKey]
      if (optionValue !== true) {
        updatedCookie += '=' + optionValue
      }
    }

    document.cookie = updatedCookie
  },
  deleteCookie(name) {
    this.setCookie(name, '', {
      'max-age': -1
    })
  }
}
export default ({ app }, inject) => {
  inject('cookies', cookies)
}
