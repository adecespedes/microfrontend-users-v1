export default async ({ app }) => {
  try {
    await app.$axios
      .get(`/Location`)
      .then((response) =>
        app.store.commit('updateLocationNodes', response.data.results)
      )
  } catch (e) {
    // eslint-disable-next-line no-console
    console.log(e.message)
  }
}
